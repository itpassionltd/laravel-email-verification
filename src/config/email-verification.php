<?php

/*
 |------------------------------------------------------------------------------
 | Laravel Email Verification
 |------------------------------------------------------------------------------
 |
 | You can set the email verification to add routes to the web routes, and / or
 | to the API routes.
 |
 |------------------------------------------------------------------------------
 */
return [
	'use_in_web_routes' => TRUE,
	'use_in_api_routes' => FALSE,
];