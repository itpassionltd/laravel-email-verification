<?php

namespace ITPassionLtd\Laravel\EmailVerification;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
	/**
	 * Add the EmailVerification middleware to the router
	 */
	public function boot()
	{
		$this->loadViewsFrom(
			__DIR__ . '/resources/views/vendor/laravel-email-verification',
			'laravel-email-verification');

		$this->publishes([
			__DIR__ . '/database/migrations' => database_path('migrations'),
		], 'email_verification-migrations');
		$this->publishes([
			__DIR__ . '/config/email-verification.php' =>
				config_path('email-verification.php')
		]);
		$this->publishes([
			__DIR__ . '/resources/views/laravel-email-verification' =>
				resource_path('views/vendor/laravel-email-verification')
		]);
	}

	/**
	 * Register the service provider
	 *
	 * @return void
	 */
	public function register()
	{
		$this->mergeConfigFrom(
			__DIR__ . '/config/email-verification.php', 'email-verification'
		);
	}

}