<?php

namespace ITPassionLtd\Laravel\EmailVerification;

use Illuminate\Contracts\Routing\Registrar as Router;

class RouteRegistrar
{
	/**
	 * The router implementation
	 *
	 * @var Router
	 */
	protected $router;

	/**
	 * RouteRegistrar constructor.
	 *
	 * @param  Router $router
	 */
	public function __construct(Router $router)
	{
		$this->router = $router;
	}

	/**
	 * Register all routes
	 */
	public function all()
	{
		$this->registration_routes();
		$this->login_routes();
	}

	/**
	 * Register the routes needed for registration
	 */
	public function login_routes()
	{
		if(config('email-verification.use_in_web_routes')) {
			$this->router->group(['middleware' => ['web', 'guest']],
				function ($router) {
					$router->post('/login', [
						'uses' => 'LoginController@login',
					]);

					$router->get('/login', [
						'as' => 'login',
						'uses' => 'LoginController@showLoginForm',
					]);
				}
			);
		}

		if(config('email-verification.use_in_api_routes')) {
			$this->router->group(['middleware' => ['api', 'guest', 'throttle']],
				function ($router) {
					$router->post('/login', [
						'uses' => 'LoginController@login',
					]);
				}
			);
		}
	}

	/**
	 * Register the routes needed for registration
	 */
	public function registration_routes()
	{
		if(config('email-verification.use_in_web_routes')) {
			$this->router->group(['middleware' => ['web', 'guest']],
				function ($router) {
					$router->get('/register', [
						'as' => 'register',
						'uses' => 'RegisterController@showRegistrationForm',
					]);

					$router->get('/register/verify/{confirmation_code}', [
						'uses' => 'RegisterController@confirm',
					]);

					$router->post('/register', [
						'uses' => 'RegisterController@register',
					]);
				}
			);
		}
		if(config('email-verification.use_in_api_routes')) {
			$this->router->group(['middleware' => ['api', 'guest', 'throttle']],
				function ($router) {
					$router->get('/register/verify/{confirmation_code}', [
						'uses' => 'RegisterController@confirm',
					]);

					$router->post('/register', [
						'uses' => 'RegisterController@register',
					]);
				}
			);
		}
	}
}