<?php

namespace ITPassionLtd\Laravel\EmailVerification;

use Illuminate\Support\Facades\Route;

class EmailVerification
{
	/**
	 * Bind the EmailVerification routes into the controller
	 *
	 * @param  callable|null	$callback
	 * @param  array			$options
	 *
	 * @return void
	 */
	public static function routes($callback = NULL, array $options = [])
	{
		$callback = $callback ?: function ($router) {
			$router->all();
		};

		$default_options = [
			'namespace' =>
				'\ITPassionLtd\Laravel\EmailVerification\Http\Controllers'
		];

		$options = array_merge($default_options, $options);

		Route::group($options, function ($router) use ($callback) {
			$callback(new RouteRegistrar($router));
		});
	}
}