<?php

namespace ITPassionLtd\Laravel\EmailVerification\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class RegisterController extends BaseController
{
	public function confirm()
	{

	}

	public function register()
	{
		$rules = [
			'name' => 'required|min:6|max:255',
			'email' => 'required|string|email|max:255|unique:users',
			'password' => 'required|string|confirmed|min:6'
		];

		$input = Input::only('name', 'email', 'password',
			'password_confirmation');

		$validator = Validator::make($input, $rules);
		if($validator->fails()) {
			return Redirect::back()->withInput()->withErrors($validator);
		}

		$confirmation_code = str_random(32);

		User::create([
			'name' => Input::get('name'),
			'email' => Input::get('email'),
			'password' => Hash::make(Input::get('password')),
			'confirmation_code' => $confirmation_code
		]);

		Mail::send('laravel-email-verification::verification_email',
			$confirmation_code, function ($message) {
				$message->to(Input::get('email'), Input::get('name'))
					->subject('project-management.tech - Please validate ' .
						'your email address');
		});

		return Redirect::home();
	}

	public function showRegistrationForm()
	{

	}
}